#[derive(Debug, Clone, Copy, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct Span {
    pub start: usize,
    pub end: usize,
}

impl Span {
    /// Create a new span
    pub fn new(start: usize, end: usize) -> Self {
        Self {
            start,
            end,
        }
    }

    /// The length of the span
    pub fn len(&self) -> usize {
        self.end - self.start
    }

    /// Returns true of the input span is a child of the method's span
    pub fn is_parent_of(&self, child: Self) -> bool {
        self.start <= child.start && self.end >= child.end
    }

    /// (Position, Actually Exists In Input)
    pub fn position(&self, input: &str) -> (Position, bool) {
        self.position_with_lines(&get_line_sizes(input))
    }

    /// (Position, Actually Exists In Input)
    pub fn position_with_lines(&self, lines: &[usize]) -> (Position, bool) {
        match position_from_index_with_lines(lines, self.start) {
            Some(s) => (s, true),
            None => (Position::new(lines.len() + 1, 1), false),
        }
    }
}

#[derive(Debug, Clone, Copy, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct Position {
    pub line: usize,
    pub column: usize,
}

impl Position {
    pub fn new(line: usize, column: usize) -> Self {
        Self {
            line,
            column,
        }
    }
}

/// Get line character lengths from a string
pub fn get_line_sizes(input: &str) -> Vec<usize> {
    if input == "" {
        return Vec::new();
    }

    let mut lines: Vec<usize> = input.lines().map(|x| x.chars().count() + 1).collect();

    if input.ends_with("\n") == false {
        let last_index = lines.len() - 1;

        lines[last_index] -= 1;
    }

    lines
}

/// Use this when you want to compute a ton of positions back to back, it doesn't need to calculate line lengths
pub fn position_from_index_with_lines(lines: &[usize], index: usize) -> Option<Position> {
    let mut sum: usize = 0;
    for (i, l) in lines.iter().enumerate() {
        let prev_sum = sum;
        sum += l;

        if sum > index {
            let mut position = Position::new(0, 0);

            position.line = i + 1;
            position.column = (index - prev_sum) + 1;

            return Some(position);
        }
    }

    None
}

/// If you need to calculate a ton of positions fast, use: position_from_index_with_lines()
pub fn position_from_index(input: &str, index: usize) -> Option<Position> {
    position_from_index_with_lines(&get_line_sizes(input), index)
}
