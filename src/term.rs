use std::io;
use crossterm::terminal;

pub struct TerminalSize {
    pub rows: u16,
    pub cols: u16,
}

impl TerminalSize {
    pub fn get() -> Result<Self, io::Error> {
        let (cols, rows) = terminal::size()?;

        return Ok(Self {
            rows,
            cols,
        });
    }
}
