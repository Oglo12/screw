pub mod tokenize;

use std::rc::Rc;
use enum_kinds::EnumKind;

use crate::syntax_tracking::{ Span, Position };

#[derive(Debug, Clone, PartialEq, EnumKind)]
#[enum_kind(TokenTypeKind)]
pub enum TokenType {
    KeywordFn,
    KeywordConst,
    KeywordImport,
    KeywordLet,
    KeywordReturn,
    KeywordBack,
    KeywordPub,
    KeywordMut,

    Ident(Rc<String>),
    String(Rc<String>),
    Character(char),
    UnsignedInteger(usize),
    SignedInteger(isize),
    Float(f64),
    Boolean(bool),

    DataTypeString,
    DataTypeCharacter,
    DataTypeUnsignedInteger(u32),
    DataTypeSignedInteger(u32),
    DataTypeFloat(u32),
    DataTypeBoolean,

    Semicolon,
    Colon,
    Comma,
    Period,
    Turbofish,
    Underscore,

    Assign,
    ImportAll,

    MathOpAdd,
    MathOpSub,
    MathOpMul,
    MathOpDiv,
    MathOpMod,
    MathOpPow,

    OpenParen,
    CloseParen,
    OpenBracket,
    CloseBracket,
    OpenCurly,
    CloseCurly,
    OpenAngle,
    CloseAngle,

    EOF,
}

impl TokenTypeKind {
    pub fn name(&self) -> String {
        format!("{:?}", self)
    }

    /// (Color, Bold, Italic)
    pub fn prompt_color(&self) -> (owo_colors::AnsiColors, bool, bool) {
        use owo_colors::AnsiColors;

        match *self {
            Self::KeywordFn => (AnsiColors::BrightRed, true, false),
            Self::KeywordConst => (AnsiColors::BrightRed, true, false),
            Self::KeywordImport => (AnsiColors::BrightRed, true, false),
            Self::KeywordLet => (AnsiColors::Red, false, false),
            Self::KeywordReturn => (AnsiColors::Red, false, false),
            Self::KeywordBack => (AnsiColors::Red, false, false),
            Self::KeywordPub => (AnsiColors::BrightRed, true, true),
            Self::KeywordMut => (AnsiColors::BrightRed, true, true),
            Self::Ident => (AnsiColors::White, false, false),
            Self::String => (AnsiColors::Green, false, false),
            Self::Character => (AnsiColors::Blue, false, false),
            Self::UnsignedInteger => (AnsiColors::Magenta, false, false),
            Self::SignedInteger => (AnsiColors::Magenta, false, false),
            Self::Float => (AnsiColors::Magenta, false, false),
            Self::Boolean => (AnsiColors::Magenta, false, false),
            Self::DataTypeString => (AnsiColors::Yellow, false, true),
            Self::DataTypeCharacter => (AnsiColors::Yellow, false, true),
            Self::DataTypeUnsignedInteger => (AnsiColors::Yellow, false, true),
            Self::DataTypeSignedInteger => (AnsiColors::Yellow, false, true),
            Self::DataTypeFloat => (AnsiColors::Yellow, false, true),
            Self::DataTypeBoolean => (AnsiColors::Yellow, false, true),
            Self::Semicolon => (AnsiColors::White, false, false),
            Self::Colon => (AnsiColors::White, false, false),
            Self::Comma => (AnsiColors::White, false, false),
            Self::Period => (AnsiColors::White, false, false),
            Self::Turbofish => (AnsiColors::White, false, false),
            Self::Underscore => (AnsiColors::White, false, false),
            Self::Assign => (AnsiColors::White, false, false),
            Self::ImportAll => (AnsiColors::BrightMagenta, false, false),
            Self::MathOpAdd => (AnsiColors::Magenta, false, false),
            Self::MathOpSub => (AnsiColors::Magenta, false, false),
            Self::MathOpMul => (AnsiColors::Magenta, false, false),
            Self::MathOpDiv => (AnsiColors::Magenta, false, false),
            Self::MathOpMod => (AnsiColors::Magenta, false, false),
            Self::MathOpPow => (AnsiColors::Magenta, false, false),
            Self::OpenParen => (AnsiColors::White, false, false),
            Self::CloseParen => (AnsiColors::White, false, false),
            Self::OpenBracket => (AnsiColors::White, false, false),
            Self::CloseBracket => (AnsiColors::White, false, false),
            Self::OpenCurly => (AnsiColors::White, false, false),
            Self::CloseCurly => (AnsiColors::White, false, false),
            Self::OpenAngle => (AnsiColors::White, false, false),
            Self::CloseAngle => (AnsiColors::White, false, false),
            Self::EOF => (AnsiColors::BrightRed, true, true),
        }
    }
}

#[derive(Debug, Clone)]
pub struct Token {
    pub kind: TokenType,
    pub span: Span,
}

impl Token {
    pub fn new(kind: TokenType, span: Span) -> Self {
        Self {
            kind,
            span,
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub struct TokenStreamState {
    offset: usize,
}

#[derive(Debug, Clone)]
pub struct TokenStream {
    tokens: Vec<Token>,
    offset: usize, // This is used for eating tokens. (eat is essentially 'self.offset += 1')
}

impl TokenStream {
    pub fn new(tokens: Vec<Token>) -> Self {
        Self {
            tokens,
            offset: 0,
        }
    }

    pub fn len(&self) -> usize {
        self.tokens.len() - self.offset
    }

    pub fn get(&self, index: usize) -> Option<&Token> {
        self.tokens.get(index + self.offset)
    }

    pub fn get_all(&self) -> &[Token] {
        &self.tokens[self.offset..self.tokens.len()]
    }

    pub fn eat(&mut self) -> Option<&Token> {
        if self.offset == self.tokens.len() {
            return None;
        }

        self.offset += 1;

        Some(self.tokens.get(self.offset - 1).unwrap())
    }

    /// Save the state of the token stream
    pub fn save_state(&self) -> TokenStreamState {
        TokenStreamState {
            offset: self.offset,
        }
    }

    /// Load a new state into the token stream and return the old state
    pub fn load_state(&mut self, state: TokenStreamState) -> TokenStreamState {
        let old_state = self.save_state();

        self.offset = state.offset;

        old_state
    }
}

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum TokenizeErrorKind {
    InvalidCharacter,
    InvalidEscapeCode,
    InvalidEscape,
    OpenEnded,
    TooMany,
    NotEnough,
}

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum TokenizeErrorDetail {
    InvalidCharacter,
    InvalidEscapeCode,
    StringEndsWithEscape,
    OpenEndedString,
    OpenEndedCharacterString,
    OpenEndedMultilineComment,
    TooManyCharactersInCharacterString,
    NoCharacterInCharacterString,
}

impl TokenizeErrorDetail {
    pub fn message(&self) -> String {
        match *self {
            Self::InvalidCharacter => String::from("Invalid character!"),
            Self::InvalidEscapeCode => String::from("Invalid escape code!"),
            Self::StringEndsWithEscape => String::from("String ends with escape!"),
            Self::OpenEndedString => String::from("String is missing an enclosing double quote!"),
            Self::OpenEndedCharacterString => String::from("Character string is missing an enclosing single quote!"),
            Self::OpenEndedMultilineComment => String::from("Multiline comment is missing an enclosing '*/'!"),
            Self::TooManyCharactersInCharacterString => String::from("Too many characters in character string!"),
            Self::NoCharacterInCharacterString => String::from("No character in character string!"),
        }
    }

    pub fn help(&self) -> Option<String> {
        #[allow(unreachable_patterns)]
        Some(match *self {
            Self::StringEndsWithEscape => String::from("Remove the extra '\\' at the end of the string!"),
            Self::OpenEndedString => String::from("Add double quote to the end of string."),
            Self::OpenEndedCharacterString => String::from("Add single quote to the end of character string."),
            Self::OpenEndedMultilineComment => String::from("Add '*/' to the end of multiline comment."),
            Self::TooManyCharactersInCharacterString => String::from("Make sure there is only one character in the character string."),
            Self::NoCharacterInCharacterString => String::from("Add a character to the character string."),
            _ => return None,
        })
    }
}

#[derive(Debug, Clone)]
pub struct TokenizeError {
    pub kind: TokenizeErrorKind,
    pub detail: TokenizeErrorDetail,
    pub position: Position,
    pub span: Span,
    pub focus: Vec<Span>,
    pub file: Option<String>,
}

impl TokenizeError {
    pub fn new(file: Option<String>, kind: TokenizeErrorKind, detail: TokenizeErrorDetail, span: Span, position: (usize, usize)) -> Self {
        Self {
            kind,
            detail,
            position: Position::new(position.0, position.1),
            span,
            focus: Vec::new(),
            file,
        }
    }

    pub fn add_focus(mut self, span: Span) -> Self {
        self.focus.push(span);

        self
    }
}

fn compress_errors(errors: Vec<TokenizeError>) -> Vec<TokenizeError> {
    let mut new_errors: Vec<TokenizeError> = Vec::new();

    for error in errors {
        if new_errors.len() == 0 {
            new_errors.push(error);
        }

        else {
            let previous = new_errors.get(new_errors.len() - 1).unwrap();

            if error.kind == previous.kind && error.detail == previous.detail && error.file == previous.file && error.span.is_parent_of(previous.span) {
                let previous = new_errors.remove(new_errors.len() - 1);

                let mut new_error = TokenizeError::new(
                    previous.file,
                    error.kind,
                    previous.detail,
                    error.span,
                    (error.position.line, error.position.column),
                );

                new_error.focus.extend(error.focus);
                new_error.focus.extend(previous.focus);

                new_errors.push(new_error);
            }

            else {
                new_errors.push(error);
            }
        }
    }

    new_errors
}

fn process_string(
    string: &str,
    string_span: Span,
    string_position: (usize, usize),
    file: Option<String>
) -> Result<String, Vec<TokenizeError>> {
    Ok(string.to_string()) // TODO
}
