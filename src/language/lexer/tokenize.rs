use super::*;

use crate::syntax_tracking::Span;

pub fn tokenize(input: &str, file: Option<String>) -> Result<TokenStream, Vec<TokenizeError>> {
    let input_len = input.chars().count();

    let mut tokens = Vec::new();
    let mut errors: Vec<TokenizeError> = Vec::new();

    let mut comment = false;
    let mut position: (usize, usize) = (1, 1);
    let mut prev_pointer: Option<usize> = None;
    let mut pointer: usize = 0;
    while pointer < input.len() {
        let character: char = input[pointer..(pointer + 1)].chars().into_iter().collect::<Vec<char>>()[0];

        match prev_pointer {
            Some(s) => position.1 = position.1.overflowing_add(pointer - s).0,
            None => (),
        };

        if character.is_whitespace() {
            if character == '\n' {
                position.0 += 1;
                position.1 = 0; // Gets incremented further down.

                comment = false; // The comment variable is for '//' comments, so when the line is over, reset it.
            }

            prev_pointer = Some(pointer);
            pointer += 1;

            continue;
        }

        if comment {
            prev_pointer = Some(pointer);
            pointer += 1;

            continue;
        }

        let mut increment_pointer = true;

        match character {
            ';' => tokens.push(Token::new(TokenType::Semicolon, Span::new(pointer, pointer + 1))),
            ',' => tokens.push(Token::new(TokenType::Comma, Span::new(pointer, pointer + 1))),
            '.' => tokens.push(Token::new(TokenType::Period, Span::new(pointer, pointer + 1))),
            '=' => tokens.push(Token::new(TokenType::Assign, Span::new(pointer, pointer + 1))),
            '(' => tokens.push(Token::new(TokenType::OpenParen, Span::new(pointer, pointer + 1))),
            ')' => tokens.push(Token::new(TokenType::CloseParen, Span::new(pointer, pointer + 1))),
            '[' => tokens.push(Token::new(TokenType::OpenBracket, Span::new(pointer, pointer + 1))),
            ']' => tokens.push(Token::new(TokenType::CloseBracket, Span::new(pointer, pointer + 1))),
            '{' => tokens.push(Token::new(TokenType::OpenCurly, Span::new(pointer, pointer + 1))),
            '}' => tokens.push(Token::new(TokenType::CloseCurly, Span::new(pointer, pointer + 1))),
            '<' => tokens.push(Token::new(TokenType::OpenAngle, Span::new(pointer, pointer + 1))),
            '>' => tokens.push(Token::new(TokenType::CloseAngle, Span::new(pointer, pointer + 1))),
            '+' => tokens.push(Token::new(TokenType::MathOpAdd, Span::new(pointer, pointer + 1))),
            '%' => tokens.push(Token::new(TokenType::MathOpMod, Span::new(pointer, pointer + 1))),
            '^' => tokens.push(Token::new(TokenType::MathOpPow, Span::new(pointer, pointer + 1))),
            '/' => {
                if tokens.len() >= 1 {
                    if tokens[tokens.len() - 1].kind == TokenType::MathOpDiv {
                        tokens.remove(tokens.len() - 1);

                        comment = true;
                    }

                    else {
                        tokens.push(Token::new(TokenType::MathOpDiv, Span::new(pointer, pointer + 1)));
                    }
                }

                else {
                    tokens.push(Token::new(TokenType::MathOpDiv, Span::new(pointer, pointer + 1)));
                }
            },
            '*' => {
                if tokens.len() >= 1 {
                    match tokens.get(tokens.len() - 1).unwrap().kind {
                        TokenType::Turbofish => {
                            tokens.push(Token::new(TokenType::ImportAll, Span::new(pointer, pointer + 1)));
                        },
                        TokenType::MathOpDiv => {
                            tokens.remove(tokens.len() - 1);

                            let begin_pointer = pointer;

                            loop {
                                pointer += 1;

                                if pointer < input_len - 1 {
                                    if &input[pointer..pointer + 2] == "*/" {
                                        pointer += 1;

                                        break;
                                    }
                                }

                                else {
                                    errors.push(TokenizeError::new(
                                        file.clone(),
                                        TokenizeErrorKind::OpenEnded,
                                        TokenizeErrorDetail::OpenEndedMultilineComment,
                                        Span::new(begin_pointer - 1, pointer),
                                        position,
                                    ).add_focus(Span::new(begin_pointer - 1, pointer)));
                                    break;
                                }
                            }
                        },
                        _ => {
                            tokens.push(Token::new(TokenType::MathOpMul, Span::new(pointer, pointer + 1)));
                        },
                    };
                }

                else {
                    tokens.push(Token::new(TokenType::MathOpMul, Span::new(pointer, pointer + 1)));
                }
            },
            ':' => {
                if tokens.len() >= 1 {
                    if tokens[tokens.len() - 1].kind == TokenType::Colon {
                        if tokens[tokens.len() - 1].span.start == pointer - 1 {
                            let removed = tokens.remove(tokens.len() - 1);

                            tokens.push(Token::new(TokenType::Turbofish, Span::new(removed.span.start, pointer + 1)));
                        }

                        else {
                            tokens.push(Token::new(TokenType::Colon, Span::new(pointer, pointer + 1)));
                        }
                    }

                    else {
                        tokens.push(Token::new(TokenType::Colon, Span::new(pointer, pointer + 1)));
                    }
                }

                else {
                    tokens.push(Token::new(TokenType::Colon, Span::new(pointer, pointer + 1)));
                }
            },
            _ => increment_pointer = false,
        };

        if increment_pointer {
            prev_pointer = Some(pointer);
            pointer += 1;

            continue;
        }

        match character {
            // Identifiers.
            '_' | 'a'..='z' | 'A'..='Z' => {
                let mut ident = String::new();

                prev_pointer = Some(pointer);

                for c in input[pointer..].chars() {
                    if c.is_alphanumeric() == false && c != '_' {
                        break;
                    }

                    ident.push(c);

                    pointer += 1;
                }

                tokens.push(Token::new(match ident.as_str() {
                    "_" => TokenType::Underscore,

                    "fn" => TokenType::KeywordFn,
                    "let" => TokenType::KeywordLet,
                    "const" => TokenType::KeywordConst,
                    "return" => TokenType::KeywordReturn,
                    "back" => TokenType::KeywordBack,
                    "import" => TokenType::KeywordImport,

                    "pub" => TokenType::KeywordPub,
                    "mut" => TokenType::KeywordMut,

                    "true" => TokenType::Boolean(true),
                    "false" => TokenType::Boolean(false),

                    "string" => TokenType::DataTypeString,
                    "char" => TokenType::DataTypeCharacter,
                    "bool" => TokenType::DataTypeBoolean,
                    _ => {
                        if ident.chars().count() > 1 && (ident.starts_with("u") || ident.starts_with("i") || ident.starts_with("f")) {
                            let mut non_numeric = String::new();
                            let mut numeric = String::new();

                            for i in ident.chars() {
                                if i.is_numeric() { numeric.push(i); }
                                else { non_numeric.push(i); }
                            }

                            match non_numeric.as_str() {
                                "u" => TokenType::DataTypeUnsignedInteger(numeric.parse().unwrap()),
                                "i" => TokenType::DataTypeSignedInteger(numeric.parse().unwrap()),
                                "f" => TokenType::DataTypeFloat(numeric.parse().unwrap()),
                                _ => TokenType::Ident(Rc::new(ident)),
                            }
                        }

                        else {
                            TokenType::Ident(Rc::new(ident))
                        }
                    },
                }, Span::new(prev_pointer.unwrap(), pointer)));
            },
            // Strings and characters. (" = String, ' = Character)
            '"' | '\'' => {
                let mut string = String::new();

                let mut at_break: usize = 0;
                let mut valid_string: bool = false;
                let mut mode: bool = false;
                for (i, c) in input[(pointer + 1)..].chars().enumerate() {
                    if c == '\\' {
                        mode = !mode;

                        if mode {
                            continue;
                        }
                    }

                    if c == character && mode == false {
                        valid_string = true;
                        at_break = i + 2;

                        break;
                    }

                    else if c == character && mode == true {
                        mode = false;
                    }

                    if mode {
                        string.push('\\');
                    }

                    string.push(c);

                    mode = false;
                }

                let string_span = Span::new(pointer, pointer + at_break);

                string = match process_string(&string, string_span, position, file.clone()) {
                    Ok(o) => o,
                    Err(e) => { errors.extend(e); break; },
                };

                if character == '\'' {
                    if string.chars().count() > 1 {
                        errors.push(TokenizeError::new(
                            file.clone(),
                            TokenizeErrorKind::TooMany,
                            TokenizeErrorDetail::TooManyCharactersInCharacterString,
                            string_span,
                            position,
                        ).add_focus(string_span));
                    }

                    else if string.chars().count() < 1 {
                        errors.push(TokenizeError::new(
                            file.clone(),
                            TokenizeErrorKind::NotEnough,
                            TokenizeErrorDetail::NoCharacterInCharacterString,
                            string_span,
                            position,
                        ).add_focus(string_span));

                        string = String::from("E");
                    }
                }

                match character {
                    '"' => match valid_string {
                        true => tokens.push(Token::new(TokenType::String(Rc::new(string)), Span::new(pointer, pointer + at_break))),
                        false => { errors.push(TokenizeError::new(
                            file,
                            TokenizeErrorKind::OpenEnded,
                            TokenizeErrorDetail::OpenEndedString,
                            Span::new(pointer, pointer + at_break),
                            position,
                        ).add_focus(Span::new(pointer, pointer + at_break))); break; },
                    },
                    '\'' => match valid_string {
                        true => tokens.push(Token::new(TokenType::Character(string.chars().into_iter().collect::<Vec<char>>()[0]), Span::new(pointer, pointer + at_break))),
                        false => { errors.push(TokenizeError::new(
                            file,
                            TokenizeErrorKind::OpenEnded,
                            TokenizeErrorDetail::OpenEndedCharacterString,
                            Span::new(pointer, pointer + at_break),
                            position,
                        ).add_focus(Span::new(pointer, pointer + at_break))); break; },
                    },
                    _ => unreachable!(),
                };

                prev_pointer = Some(pointer);
                pointer += at_break;
            },
            // Numbers.
            '-' | '0'..='9' => {
                if tokens.len() >= 1 {
                    if let Some(s) = tokens.get(tokens.len() - 1) {
                        match s.kind {
                            TokenType::UnsignedInteger(_) | TokenType::SignedInteger(_) | TokenType::Float(_) => {
                                tokens.push(Token::new(TokenType::MathOpSub, Span::new(pointer, pointer + 1)));
                                pointer += 1;
                                continue;
                            },
                            _ => (),
                        };
                    }
                }

                let mut number = String::new();

                prev_pointer = Some(pointer);

                for (i, c) in input[pointer..].chars().enumerate() {
                    if c.is_numeric() == false && ((i == 0 && c != '-') || i != 0) {
                        break;
                    }

                    number.push(c);

                    pointer += 1;
                }

                match character {
                    '-' => {
                        if number == "-" {
                            tokens.push(Token::new(TokenType::MathOpSub, Span::new(prev_pointer.unwrap(), prev_pointer.unwrap() + 1)));
                        }

                        else {
                            tokens.push(Token::new(TokenType::SignedInteger(number.parse().unwrap()), Span::new(prev_pointer.unwrap(), pointer)));
                        }
                    },
                    _ => {
                        let mut is_float = false;

                        if tokens.len() >= 2 {
                            let first_num = tokens.get(tokens.len() - 2).unwrap();
                            let period = tokens.get(tokens.len() - 1).unwrap();

                            match first_num.kind {
                                TokenType::SignedInteger(_) | TokenType::UnsignedInteger(_) => {
                                    match period.kind {
                                        TokenType::Period => {
                                            if first_num.span.end == period.span.start && period.span.end == prev_pointer.unwrap() {
                                                is_float = true;

                                                tokens.remove(tokens.len() - 1);
                                                let first_num = tokens.remove(tokens.len() - 1);

                                                let float = format!("{}.{}", match first_num.kind {
                                                    TokenType::UnsignedInteger(n) => n.to_string(),
                                                    TokenType::SignedInteger(n) => n.to_string(),
                                                    _ => unreachable!(),
                                                }, number);

                                                tokens.push(Token::new(TokenType::Float(float.parse().unwrap()), Span::new(first_num.span.start, pointer)));
                                            }
                                        },
                                        _ => (),
                                    };
                                },
                                _ => (),
                            };
                        }

                        match is_float {
                            false => tokens.push(Token::new(TokenType::UnsignedInteger(number.parse().unwrap()), Span::new(prev_pointer.unwrap(), pointer))),
                            true => (),
                        };
                    },
                };
            },
            _ => { errors.push(TokenizeError::new(
                file,
                TokenizeErrorKind::InvalidCharacter,
                TokenizeErrorDetail::InvalidCharacter,
                Span::new(pointer, pointer + 1),
                position,
            ).add_focus(Span::new(pointer, pointer + 1))); break; },
        };
    }

    tokens.push(Token::new(TokenType::EOF, Span::new(pointer, pointer + 1)));

    if errors.len() > 0 {
        return Err(compress_errors(errors));
    }

    Ok(TokenStream::new(tokens))
}
