mod prompt;
mod term;
mod misc;
mod system;
mod language;
mod syntax_tracking;

use std::process::ExitCode;

use crate::prompt::*;
use crate::prompt::config::PromptConfig;

fn main() -> ExitCode {
    let mut state = PromptState::new();
    let config = PromptConfig {
        ..Default::default()
    };

    loop {
        let input = match prompt(&mut state, &config) {
            Ok(o) => match o {
                PromptReturnType::String(string) => string,
                PromptReturnType::CtrlC => {
                    println!("[SIGNAL]: Received ^C, ignoring...");

                    continue;
                },
                PromptReturnType::CtrlD => break,
            },
            Err(_) => return ExitCode::FAILURE,
        };

        if input.chars().count() > 0 {
            println!("[OUT]: {input}");
        }
    }

    ExitCode::SUCCESS
}
