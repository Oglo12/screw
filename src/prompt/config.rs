use hashbrown::HashMap;
use owo_colors::OwoColorize;

use crate::system;

pub struct PromptConfigEvaluated<'a> {
    pub prompt_left: String,
    pub prompt_left_fail: String,
    pub prompt_right: String,
    pub prompt_right_fail: String,
    pub window_title: String,
    pub ctrlc_display: &'a str,
    pub ctrld_display: &'a str,
    pub color_input: bool,
}

pub struct PromptConfig {
    pub prompt_left: String,
    pub prompt_left_fail: Option<String>,
    pub prompt_right: String,
    pub prompt_right_fail: Option<String>,
    pub window_title: String,
    pub ctrlc_display: String,
    pub ctrld_display: String,
    pub color_input: bool,
}

impl PromptConfig {
    pub fn evaluate(&self) -> PromptConfigEvaluated {
        let prompt_left = Self::eval_prompt_text(&self.prompt_left);
        let prompt_right = Self::eval_prompt_text(&self.prompt_right);

        let prompt_left_fail = Self::eval_prompt_text_optional(&self.prompt_left_fail, prompt_left.clone());
        let prompt_right_fail = Self::eval_prompt_text_optional(&self.prompt_right_fail, prompt_right.clone());

        let window_title = Self::eval_prompt_text(&self.window_title);

        PromptConfigEvaluated {
            prompt_left,
            prompt_left_fail,
            prompt_right,
            prompt_right_fail,
            window_title,
            ctrlc_display: &self.ctrlc_display,
            ctrld_display: &self.ctrld_display,
            color_input: self.color_input,
        }
    }

    fn eval_prompt_text<S: ToString>(raw: S) -> String {
        let vars = Self::get_vars();

        let mut new_text = raw.to_string();

        for k in vars.keys() {
            let v = vars.get(k).unwrap();

            new_text = new_text.replace(&format!("{}{}{}", "${", k, "}"), v);
        }

        new_text
    }

    fn eval_prompt_text_optional<S: ToString>(raw: &Option<S>, fallback_pre_evaluated: S) -> String {
        Self::eval_prompt_text(match *raw {
            Some(ref s) => s.to_string(),
            None => return fallback_pre_evaluated.to_string(),
        })
    }

    fn get_vars() -> HashMap<String, String> {
        let null = String::from("NULL");

        let mut map: HashMap<String, String> = HashMap::new();

        map.insert("username".to_string(), system::username());
        map.insert("hostname".to_string(), system::hostname());

        map.insert("location_raw".to_string(), match std::env::current_dir() {
            Ok(o) => o.to_string_lossy().to_string(),
            Err(_) => null.clone(),
        });

        map.insert("location".to_string(), match std::env::current_dir() {
            Ok(o) => o.to_string_lossy().replace(&format!("/home/{}", system::username()), "~"),
            Err(_) => null.clone(),
        });

        map
    }

    #[allow(unused)]
    pub fn config_extremely_minimal() -> Self {
        Self {
            prompt_left: String::from("${username}@${hostname}: "),
            prompt_left_fail: Some(String::from("${username}@${hostname} X: ")),
            prompt_right: String::new(),
            prompt_right_fail: None,
            window_title: String::from("${username}@${hostname} in ${location}"),
            ctrlc_display: String::from("^C"),
            ctrld_display: String::from("^D"),
            color_input: false,
        }
    }

    #[allow(unused)]
    pub fn config_minimal() -> Self {
        Self {
            prompt_left: String::from("[${username}@${hostname}] > "),
            prompt_left_fail: Some(String::from("[${username}@${hostname}] X ")),
            prompt_right: String::from("[${location}]"),
            prompt_right_fail: None,
            window_title: String::from("${username}@${hostname} in ${location}"),
            ctrlc_display: String::from("^C"),
            ctrld_display: String::from("^D"),
            color_input: false,
        }
    }

    #[allow(unused)]
    pub fn config_fancy() -> Self {
        let bl = "[".bright_cyan();
        let br = "]".bright_cyan();
        let at = "@".bright_cyan();
        let dr = "$".bright_cyan();
        let cl = ":".bright_cyan();
        let username = "${username}".bright_green();
        let hostname = "${hostname}".bright_magenta();
        let location = "${location}".bright_blue();
        let success_face = "^-^".bright_green();
        let failure_face = "O-O".bright_red();

        Self {
            prompt_left: format!("{bl}{username}{at}{hostname}{br} {dr}{cl} "),
            prompt_left_fail: None,
            prompt_right: format!("{success_face} {bl}{location}{br}"),
            prompt_right_fail: Some(format!("{failure_face} {bl}{location}{br}")),
            window_title: String::from("[${username}@${hostname}] in [${location}]"),
            ctrlc_display: format!("{}", "^C".bright_red().bold()),
            ctrld_display: format!("{}", "^D".bright_red().bold()),
            color_input: true,
        }
    }
}

impl Default for PromptConfig {
    fn default() -> Self {
        // Self::config_extremely_minimal()
        // Self::config_minimal()
        Self::config_fancy()
    }
}
